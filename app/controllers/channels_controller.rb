class ChannelsController < ApplicationController
  def index
    @channels = Channel.all
    @user = current_user
  end

  def new
    @channel = Channel.new
  end

  def create
    @channel = Channel.new(channel_params)
    if @channel.save
      redirect_to @channel
    else
      flash[:error] = "Channel topic duplicated!"
      redirect_to :back
    end
  end

  def show
    @channel = Channel.find(params[:id])
    @message = Message.new
    @user_channelship = current_user.user_channelships.find_by(channel_id: params[:id])
    if @user_channelship.nil?
      @join = false
    else
      @join = true
      @user_channel = current_user.channels.find_by(id: params[:id])
      @read = @user_channelship.read
      @user_channelship.update :read => @channel.messages.last.try(:id) || 0
    end
  end

  def join_channel
    if current_user.channels.find_by(id: params[:id]).nil?
      current_user.user_channelships.create(channel_id: params[:id])
    end
    @channel = Channel.find(params[:id])
    redirect_to @channel
  end

  private

  def channel_params
    params.require(:channel).permit(:topic)
  end

end
