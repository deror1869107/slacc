module ApplicationHelper
  def channel_lists
    current_user.channels.order(:topic)
  end
end
