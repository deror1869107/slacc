module ChannelsHelper
  def unread user, channel
    return user.user_channelships.find_by(:channel_id => channel.id).read < (channel.messages.last.try(:id) || 0)
  end
end
