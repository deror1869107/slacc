// app/assets/javascripts/channels/messages.js

var message_markup = [
  '<div class="day-msgs">',
  '<ts-message class="message">',
  '<div class="message-gutter">',
    '<img src="/assets/doge.jpg" alt=doge>',
  '</div>',
  '<div class="message-content">',
    '<div class="message-content-header">${username}</div>',
    '<div class="message-content-time">${time}</div>',
    '<div class="message-body"><p>{{html message}}</p></div>',
  '</div>',
  '</ts-message>',
  '</div>'
].join("\n");

App.messagesSubscription = App.cable.subscriptions.create('MessagesChannel', {
  received: function(data) {
    this.read_update()
    channel_list = $("[data-channel-list-id=" + "'" + data.channel_id + "'" + "]");
    channel_list.children().first().addClass('unread-channel');
    $.tmpl(message_markup, data).appendTo($("[data-channel-id='" + data.channel_id + "']").find(".day-container"));
    if(Math.abs($('.row-fluid').scrollTop()+$('.row-fluid').prop('clientHeight')-$('.row-fluid').prop('scrollHeight'))
        < $('#msgs-div').children().last().children().last().outerHeight()){
        $('.row-fluid').animate({scrollTop :$('.row-fluid').prop('scrollHeight')},500);
    }
  },
  read_update: function() {
    if($("[data-channel-id]").data()) {
      var channelId = $("[data-channel-id]").data().channelId
      this.perform("read_update", {channel_id: channelId})
    }
  }
});

$(document).on('turbolinks:load', function() {
  submitNewMessage();
});

function submitNewMessage() {
  $('textarea#message-input').keydown(function(event) {
    if(event.keyCode === 13 && !event.shiftKey) {
      var msg = event.target.value.trim()
      if(msg != ""){
        var channelId = $("[data-channel-id]").data().channelId
        App.messagesSubscription.send({message: msg, channel_id: channelId})
      }
      $('[data-textarea="message"]').val('')
      return false
    }
  });
}

